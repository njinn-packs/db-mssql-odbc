import pyodbc


class ODBCBase:
    mssqlodbcconnection = {}
    cnxn = None
    cursor = None

    def connect(self):
        connection_string = ""

        if self.mssqlodbcconnection.get("custom_connection"):
            connection_string = self.mssqlodbcconnection.get("custom_connection")            
            print("Establishing connection using custom connection string.")

        else:
            connection_string += f"Driver={{{self.mssqlodbcconnection.get('driver','ODBC Driver 17 for SQL Server')}}};"
            connection_string += f"Server={self.mssqlodbcconnection.get('server')};"
            connection_string += f"Database={self.mssqlodbcconnection.get('database')};"
            connection_string += f"Uid={self.mssqlodbcconnection.get('uid')};"
            connection_string_display = connection_string
            connection_string += f"Pwd={self.mssqlodbcconnection.get('pwd')};"
            connection_string_display += "Pwd=******"            
            print("Establishing connection using connection string:")
            print(connection_string_display)

        print()
            
        self.cnxn = pyodbc.connect(connection_string, timeout=30)
        self.cursor = self.cnxn.cursor()

    def disconnect(self):
        self.cnxn.close()
        print("Connection closed")


class RunQuery(ODBCBase):
    query = ""

    def run(self):
        if not self.query:
            raise Exception("No query defined to execute")

        result = {}
        resultsets = []
        self.connect()
        try:
            with self.cnxn:
                print("Executing query.")

                self.cursor.execute(self.query)
                i = 1
                while True:
                    print(f"Processing Resultset({i})")
                    try:
                        rows = self.cursor.fetchall()
                        print(f"{len(rows)} row(s) retrieved")
                        rows_list = [[col for col in row] for row in rows]
                        resultsets.append({"rows": rows_list})
                    except pyodbc.ProgrammingError:
                        print("Query didn't return a resultset")

                    rows = self.cursor.rowcount
                    if rows != -1:
                        print(f"{rows} row(s) affected")

                    i += 1
                    if not self.cursor.nextset():
                        break

            if len(resultsets) == 1:
                return resultsets[0]
            if len(resultsets) == 0:
                return {}
            i = 0
            for set in resultsets:
                result[f"rows_{i}"] = set["rows"]
                i += 1
            return result

        finally:
            self.disconnect()
